#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"

using namespace std;

class Pentagono : public FormaGeometrica{
	private:
		float apotema;	
	public:
		Pentagono(string tipo, float base, float apotema);
		
		float get_apotema();
		void set_apotema(float apotema);
		float calcula_area();
		float calcula_perimetro();
		void imprime_dados();

};

#endif
