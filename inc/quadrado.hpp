#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"

using namespace std;

class Quadrado : public FormaGeometrica{
	public:
		Quadrado(string tipo, float base, float altura);
};

#endif
