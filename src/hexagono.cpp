#include "hexagono.hpp"
#include <iostream>

using namespace std;

Hexagono::Hexagono(string tipo, float base){
	set_tipo("Hexágono");
	set_base(base);
}

float Hexagono::calcula_area(){
	float a;
	a = 3*(get_base()*get_base())*1.7320508076;
	return a/2;
}

float Hexagono::calcula_perimetro(){
	return get_base()*6;
}

void Hexagono::imprime_dados(){
	cout << "Tipo: " << get_tipo() << endl;
	cout << "Área: " << calcula_area() << endl;
	cout << "Perimetro: " << calcula_perimetro() << endl;
}
