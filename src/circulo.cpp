#include "circulo.hpp"
#include <iostream>

using namespace std;

float p=3.14159265;

Circulo::Circulo(string tipo, float raio){
	set_tipo("Círculo");
	set_raio(raio);
}

float Circulo::get_raio(){
	return raio;
}

void Circulo::set_raio(float raio){
	this->raio=raio;
}

float Circulo::calcula_area(){
	float c;
	c = get_raio();
	c = c*c;	
	return c*p;
}

float Circulo::calcula_perimetro(){
	float c;
	c = get_raio();	
	return 2*p*c;
}

void Circulo::imprime_dados(){
	cout << "Tipo: " << get_tipo() << endl;
	cout << "Área: " << calcula_area() << endl;
	cout << "Perimetro: " << calcula_perimetro() << endl;
}
	
