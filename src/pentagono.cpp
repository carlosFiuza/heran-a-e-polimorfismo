#include "pentagono.hpp"
#include <iostream>

using namespace std;

Pentagono::Pentagono(string tipo, float base, float apotema){
	set_tipo("Pentágono");
	set_base(base);
	set_apotema(apotema);
}

float Pentagono::get_apotema(){
	return apotema;
}

void Pentagono::set_apotema(float apotema){
	this->apotema=apotema;
}

float Pentagono::calcula_area(){
	float p, a;
	p = get_base();
	p = p*5;
	a = get_apotema();	
	return (a*p)/2;
}

float Pentagono::calcula_perimetro(){
	return get_base()*5;
}

void Pentagono::imprime_dados(){
	cout << "Tipo: " << get_tipo() << endl;
	cout << "Área: " << calcula_area() << endl;
	cout << "Perimetro: " << calcula_perimetro() << endl;
}
