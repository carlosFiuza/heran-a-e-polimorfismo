#include <iostream>
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "paralelogramo.hpp"
#include "circulo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"
#include <vector>
#include <string>

using namespace std;

string getString(){
    string valor;
    getline(cin, valor);
    return valor;
}

template <typename H>

H getInput(){
    while(true){
    H valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    	}
    else{
        cin.ignore(32767,'\n');
        return valor;
   	 }
    }
}

int main(){

	vector<FormaGeometrica *> forma;

	int com=-1;
	float base, altura, apotema, raio;
	string tipo;


	while(com!=0){
		cout << "Digite 1 para quadrado, 2 para triangulo, 3 para paralelogramo, 4 para circulo, 5 para pentagono, 6 para hexagono e 0 para sair e imprimir" << endl;					
		com = getInput<int>();		
		try {
		switch(com){
			case 1:

				//base = getInput<float>();
				base = 10;
				//altura = getInput<float>();
				altura = 10;
				forma.push_back(new Quadrado(tipo, base, altura));

				break;


			case 2:

				//base = getInput<float>();
				base = 4;
				//altura = getInput<float>();
				altura = 3;
				forma.push_back(new Triangulo(tipo, base, altura));
				break;

			case 3:

        	        	//base = getInput<float>();
				base = 10;
        	        	//altura = getInput<float>();
				altura = 10;
	                	forma.push_back(new Paralelogramo(tipo, base, altura));
				break;

			case 4:

                        	//raio = getInput<float>();
				raio = 25;
                        	forma.push_back(new Circulo(tipo, raio));
				break;

			case 5:

				//base = getInput<float>();
				base = 5;
				//apotema = getInput<float>();
				apotema = 3;
				forma.push_back(new Pentagono(tipo, base, apotema));
				break;

			case 6:

				//base = getInput<float>();
				base = 6;
				forma.push_back(new Hexagono(tipo, base));
				break;
			case 0:
				break;
			default:
				cout << "Número inválido" << endl;
			}
		} catch(int erro){
			cout << "base ou altura menor que 0" << endl;
			}
		}

	for (FormaGeometrica * f: forma){
		f->imprime_dados();
		cout << "-------------------------------------------------" << endl;
	}

	return 0; 
}

