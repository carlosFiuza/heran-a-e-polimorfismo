#include "triangulo.hpp"
#include <iostream>

using namespace std;

Triangulo::Triangulo(string tipo, float base, float altura){
	set_tipo("Triângulo");
	set_base(base);
	set_altura(altura);
}

float Triangulo::calcula_area(){
	float t, a;
	t = get_base();	
	a = get_altura();
	return (t*a)/2;
}

float Triangulo::calcula_perimetro(){
	float a = get_base();
	return a*3;
}

void Triangulo::imprime_dados(){
	cout << "Tipo: " << get_tipo() << endl;
	cout << "Área: " << calcula_area() << endl;
	cout << "Perimetro: " << calcula_perimetro() << endl;
}  

